<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=# VizworX Renovate Configuration _(${name})_) -->
# VizworX Renovate Configuration _(@vizworx/renovate-config)_
<!-- AUTO-GENERATED-CONTENT:END -->

[![NPM Version][package-version-svg]][package-url]
[![License][license-image]][license-url]

<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=> ${description}&unknownTxt= ) -->
> A renovate config with common settings for VizworX
<!-- AUTO-GENERATED-CONTENT:END -->

This is a collection of [Renovate config presets](https://docs.renovatebot.com/config-presets/) that are used on [VizworX](https://vizworx.com) projects. You can extend the base config, or any of the named configs.

## Getting Started - Enabling Renovate on your repository

* [for GitHub](https://docs.renovatebot.com/install-github-app/)
* [for GitLab](https://docs.renovatebot.com/install-gitlab-app/)
  * go to [Renovate Dashboard](https://app.renovatebot.com/dashboard) to add your project,
  * accept Renovate Bot PR

## Usage

You don't need to install this package, since Renovate will automatically pull it in if you extend `@vizworx` or `@vizworx:subPackageName`

**Recommended configuration for projects**:

This will enable all of the configurations listed below, however you can use them individually if you would prefer.

`renovate.json`

```json
{
  "extends": [
    "@vizworx"
  ]
}
```

### Renovate Configuration

`renovate.json`

**Release Notes** :

Enables showing the release notes from GitHub

```json
{
  "extends": [
    "@vizworx:releaseNotes"
  ]
}
```

**Labels**:

Automatically apply common VizworX labels such as `🤖 Dependency` to PRs

```json
{
  "extends": [
    "@vizworx:labels"
  ]
}
```

**Node**:

Always update to the latest LTS of Node

```json
{
  "extends": [
    "@vizworx:node"
  ]
}
```

**Automerge**:

Pinned PRs should be automerged, and should skip the CI system

```json
{
  "extends": [
    "@vizworx:automerge"
  ]
}
```

**Stability**:

Wait a few days to help ensure new dependency versions are stable, and that new patches aren't being released.

* Major - Wait 7 days
* Minor - Wait 2 days
* Patch - Wait 2 days

```json
{
  "extends": [
    "@vizworx:stability"
  ]
}
```

**Show All**:

Remove limits on the number of dependency PRs that can exist at one time, or be created in one hour, and enable the Master Issue to keep track of upcoming (unstable) PRs

```json
{
  "extends": [
    "@vizworx:showAll"
  ]
}
```

**Group - React**:

Group all React-related packages together to ensure we don't have a mismatch between closely linked dependencies such as `react` and `react-dom`, or `react-dom` and `@hot-loader/react-dom`.

```json
{
  "extends": [
    "@vizworx:groupReact"
  ]
}
```

**Group - All**:

Enable commonly used package groups

```json
{
  "extends": [
    "@vizworx:groupAll"
  ]
}
```

**Outside Business Hours**:

Only run Renovate outside of business hours, to reduce the amount of PR noise and CI load generated during the day. Renovate will run before 5am and after 10pm on weekdays, and any time during the weekends.

```json
{
  "extends": [
    "@vizworx:outsideBusinessHours"
  ]
}
```

**Quiet Noisy Packages**:

Some packages, such as the [AWS SDK](https://www.npmjs.com/package/aws-sdk) release a new package on a frequent schedule. This will limit them to only update once a week (on Monday before 3am).

```json
{
  "extends": [
    "@vizworx:quietNoisyPackages"
  ]
}
```

## License
<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[${license}][license-url] © VizworX) -->
[MIT][license-url] © VizworX
<!-- AUTO-GENERATED-CONTENT:END -->

<!-- VARIABLES -->

<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[package-version-svg]: https://img.shields.io/npm/v/${name}.svg?style=flat-square) -->
[package-version-svg]: https://img.shields.io/npm/v/@vizworx/renovate-config.svg?style=flat-square
<!-- AUTO-GENERATED-CONTENT:END -->
<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[package-url]: https://www.npmjs.com/package/${name}) -->
[package-url]: https://www.npmjs.com/package/@vizworx/renovate-config
<!-- AUTO-GENERATED-CONTENT:END -->
<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[license-image]: https://img.shields.io/badge/license-${license}-green.svg?style=flat-square) -->
[license-image]: https://img.shields.io/badge/license-MIT-green.svg?style=flat-square
<!-- AUTO-GENERATED-CONTENT:END -->
[license-url]: ./LICENSE
